package com.bawali.stayahead;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    @Bind(R.id.input_email) EditText _emailText;
    @Bind(R.id.input_name) EditText _nameText;
    @Bind(R.id.input_hotel) EditText _hotelText;
    @Bind(R.id.btn_login) Button _loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        _loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = _emailText.getText().toString();
                String name = _nameText.getText().toString();
                String hotel = _hotelText.getText().toString();
                PreferenceManager.getDefaultSharedPreferences(
                        LoginActivity.this).edit().putString("email",
                        email).apply();
                PreferenceManager.getDefaultSharedPreferences(
                        LoginActivity.this).edit().putString("name",
                        name).apply();
                PreferenceManager.getDefaultSharedPreferences(
                        LoginActivity.this).edit().putString("hotel",
                        hotel).apply();

                Intent intent = new Intent(LoginActivity.this, ChatActivity.class);
                startActivity(intent);
            }
        });

    }
}
