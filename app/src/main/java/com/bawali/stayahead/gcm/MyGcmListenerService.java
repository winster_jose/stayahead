/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bawali.stayahead.gcm;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.bawali.stayahead.ChatActivity;
import com.bawali.stayahead.app.Config;
import com.bawali.stayahead.utils.Database;
import com.bawali.stayahead.vo.Message;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class MyGcmListenerService extends FirebaseMessagingService {

    private static final String TAG = "MyGcmListenerService";
    private NotificationUtils notificationUtils;
    private Database database = new Database(this);

    /**
     * Called when message is received.
     *
     * @param message
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage message) {
        Log.d(TAG, "Received a message");

        String from = message.getFrom();
        Map data = message.getData();

        if (from.startsWith("/topics/")) {
            // message received from some topic.
        } else {
            // normal downstream message.
        }

        // [START_EXCLUDE]
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */
        processMessage(data);
        // [END_EXCLUDE]
    }
    // [END receive_message]

    /**
     * Processing chat room push message
     * this message will be broadcasts to all the activities registered
     * */
    private void processMessage(Map data) {
        Log.d(TAG, data.toString());
        Message message = new Message();
        message.setData((String)data.get("data"));
        message.setOwner((String)data.get("user"));
        message.setId((String)data.get("id"));
        message.setCreatedAt(Long.valueOf((String)data.get("time")));

        // verifying whether the app is in background or foreground
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {

            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils();
            notificationUtils.playNotificationSound();
        } else {
            database.insertChat(message);
            // app is in background. show the message in notification try
            Intent resultIntent = new Intent(getApplicationContext(), ChatActivity.class);
            //resultIntent.putExtra("product_id", message.getProductId());
            //resultIntent.putExtra("contact_id", message.getContactId());
            //ProductContact contact = database.getProductContact(message.getProductId(), message.getContactId());
            //resultIntent.putExtra("name", contact.name);
            showNotificationMessage(getApplicationContext(), message.getOwner(), message.getData(),
                    message.getCreatedAt(), resultIntent);
        }

    }

    /**
     * Showing notification with text only
     * */
    private void showNotificationMessage(Context context, String title, String message,
                                         long timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     * */
    private void showNotificationMessageWithBigImage(Context context, String title, String message,
                                                     long timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }

    @Override
    public void onMessageSent(String e){
        Log.d(TAG, e);
    }

}
