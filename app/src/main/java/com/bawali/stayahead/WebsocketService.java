package com.bawali.stayahead;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bawali.stayahead.app.AppController;
import com.bawali.stayahead.app.Config;
import com.bawali.stayahead.utils.Database;
import com.bawali.stayahead.utils.WebService;
import com.bawali.stayahead.vo.Message;
import com.koushikdutta.async.callback.CompletedCallback;
import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.AsyncHttpGet;
import com.koushikdutta.async.http.AsyncHttpRequest;
import com.koushikdutta.async.http.WebSocket;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by wjose on 7/12/2016.
 */
public class WebsocketService extends Service {

    public static final String TAG = "WebsocketService";
    private final IBinder mBinder = new LocalBinder();
    private Database database;

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        WebsocketService getService() {
            // Return this instance of LocalService so clients can call public methods
            return WebsocketService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Service Started");
        connectWebSocket();
        database = new Database(this);
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Service Destroyed");
    }

    private WebSocket socket = null;
    private void connectWebSocket(){
        AsyncHttpRequest get = new AsyncHttpGet(Config.WEBSOCKET_SERVER_URL);
        AsyncHttpClient.getDefaultInstance().websocket(get, null, new AsyncHttpClient.WebSocketConnectCallback() {
            @Override
            public void onCompleted(Exception ex, WebSocket webSocket) {
                if (ex != null) {
                    ex.printStackTrace();
                    Log.d(TAG, "Exception in websocket connection");
                    return;
                }
                socket=webSocket;
                webSocket.setStringCallback(new WebSocket.StringCallback() {
                    public void onStringAvailable(String s) {
                        try {
                            JSONObject socketMessage = new JSONObject(s);
                            if(socketMessage!=null){
                                if(socketMessage.has("connection_id")) {
                                    WebService.updateSocketId(WebsocketService.this,
                                            socketMessage.getString("connection_id"));

                                    new Timer().scheduleAtFixedRate(new TimerTask(){
                                        @Override
                                        public void run(){
                                            Log.i("tag", "Ping::"+socket.isOpen());
                                            socket.send("ping");
                                        }
                                    },30000,30000);

                                } else if(socketMessage.has("data")) {
                                    Log.d(TAG, "got a chat from friend:: "+s);
                                    String user = socketMessage.getString("user");
                                    String data = socketMessage.getString("data");
                                    String type = socketMessage.getString("type");
                                    String id = socketMessage.getString("id");
                                    long time = socketMessage.getLong("time");
                                    Message message = new Message(id, data, type,
                                            user, user, time);
                                    Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                                    pushNotification.putExtra("message", message);
                                    LocalBroadcastManager.getInstance(WebsocketService.this).sendBroadcast(pushNotification);
                                }
                            } else {
                                Log.d(TAG, "ignoring the message from server:: "+s);
                            }
                        } catch(final JSONException e) {
                            e.printStackTrace();
                            Log.d(TAG, "JSON excepion while processing data");
                        }
                    }
                });
                webSocket.setClosedCallback(new CompletedCallback() {
                    @Override
                    public void onCompleted(Exception ex) {
                        Log.d(TAG, "connection closed");
                    }
                });
                webSocket.setEndCallback(new CompletedCallback() {
                    @Override
                    public void onCompleted(Exception ex) {
                        ex.printStackTrace();
                        Log.d(TAG, "connection closed"+ex.getMessage());
                    }
                });
            }
        });
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Log.d(TAG, "Going Offline");
        socket.close();
        WebService.offline(WebsocketService.this);
    }

    public void sendMessage(Message chat) {
        JSONObject json = new JSONObject();
        try {
            String user = PreferenceManager.getDefaultSharedPreferences(WebsocketService.this).getString("email", "");
            String hotel = PreferenceManager.getDefaultSharedPreferences(WebsocketService.this).getString("hotel", "");
            String access_token = PreferenceManager.getDefaultSharedPreferences(WebsocketService.this).getString("access_token", "");
            json.put("user", user);
            json.put("token", access_token);
            json.put("to", hotel);
            json.put("data", chat.getData());
            json.put("type", chat.getType());
            json.put("id", chat.getId());
            Log.d(TAG, json.toString());
            socket.send(json.toString());
        } catch(JSONException e) {
            e.printStackTrace();
        }

    }
}
