package com.bawali.stayahead.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.bawali.stayahead.vo.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wjose on 06/07/2016.
 */
public class Database  extends SQLiteOpenHelper {

    private static final String TAG = "Database";

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "database.db";

    private static final String SQL_DELETE_MESSAGES = "DROP TABLE IF EXISTS messages";
    public static final String SQL_TRUNCATE_MESSAGES = "DELETE FROM messages";

    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE messages (message_id TEXT, " +
                        "data TEXT, type TEXT, owner TEXT, created_at INTEGER, " +
                        "PRIMARY KEY (message_id))"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        clearDatabase(db);
        onCreate(db);
    }

    public void clearDatabase(SQLiteDatabase db) {
        db.execSQL(SQL_DELETE_MESSAGES);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    @Override
    protected void finalize() throws Throwable {
        this.close();
        super.finalize();
    }

    public long insertChat(Message message) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("owner", message.getOwner());
        contentValues.put("message_id", message.getId());
        contentValues.put("data", message.getData());
        contentValues.put("type", message.getType());
        contentValues.put("created_at", message.getCreatedAt());
        Log.d(TAG, "insert chat:: "+contentValues.toString());
        return db.insert("messages", null, contentValues);
    }

    public long deleteChat(Message message) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("messages", "message_id=?",
                new String[]{message.getId()});
    }

    public List<Message> getChatHistory() {
        List<Message> messages = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select message_id, owner, data, type, created_at from messages" +
                " order by created_at",
                new String[]{});
        res.moveToFirst();
        Message message = null;
        while (!res.isAfterLast()) {
            message = new Message();
            message.setOwner(res.getString(res.getColumnIndex("owner")));
            message.setData(res.getString(res.getColumnIndex("data")));
            message.setId(res.getString(res.getColumnIndex("message_id")));
            message.setType(res.getString(res.getColumnIndex("type")));
            message.setCreatedAt(res.getLong(res.getColumnIndex("created_at")));
            messages.add(message);
            res.moveToNext();
        }
        return messages;
    }

    public void execScript(String script) {
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL(script);
    }

}
